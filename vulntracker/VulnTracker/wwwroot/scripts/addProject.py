#! /usr/bin/env python3
import sqlite3, time, os, json
con = sqlite3.connect("../data/libraries.db")
cur = con.cursor()

def checkForNewProjects():
    for file in os.listdir(path):
        if file.lower().endswith(".json"):
            createProjectFromJSON(file.split(".")[0])
        elif file.lower().endswith(".xml"):
            createProjectFromXML(file.split(".")[0])
        else:
            print(f"Format not supported: {file}")
            os.remove(path + "/" + file)


def createProjectFromJSON(projectFile):
    print(f"Found JSON: {projectFile}")
    #os.remove(path + f"/{projectFile}.json")


def createProjectFromXML(projectFile):
    print(f"Found XML: {projectFile}")


def createWebpage(projectFile):
    csFile = open(f"../../Pages/Projects/{projectFile}.cshtml.cs", 'w')
    csFile.write("using Microsoft.AspNetCore.Mvc;\nusing Microsoft.AspNetCore.Mvc.RazorPages;\n\nnamespace VulnTracker.Pages{\n" + f'public class {projectFile}Model : PageModel' + "{\npublic void OnGet(){}\n}\n" + "}")
    csFile.close()
    htmlFile = open(f"../../Pages/Projects/{projectFile}.cshtml", 'w')
    htmlFile.write(f"@page\n@model VulnTracker.Pages.{projectFile}Model\n@inject SignInManager<IdentityUser> SignInManager\n" + "@{" + f'\n    ViewData["Title"] = "{projectFile}";\n' + "}\n\n@if (SignInManager.IsSignedIn(User)){\n\n")
    htmlFile.write(f"<h1>Hello World!</h1><br /><p>Created from {projectFile}.json</p><br /><hr /><br />\n\n\n")
    htmlFile.write('}else{Response.Redirect("/");}')
    htmlFile.close()


def testCase(projectFile):
    bomFile = json.load(open(f"../data/projectFiles/testBom.json"))
    project = {}
    application = {}
    components = {}
    if bomFile["bomFormat"] != "CycloneDX":
        print("Invalid Bom Format")
        return
    else:
        project.update({"projectFile": projectFile})
        project.update({"bomFormat": bomFile["bomFormat"]})
        project.update({"bomVersion": bomFile["version"]})
    for items in bomFile["components"]:
        if items["type"] == "application":
                application.update({"Type": items["type"]}) #Required in CycloneDX
                application.update({"Name": items["name"]}) #Required in CycloneDX
                try: application.update({"Version": items["version"]})
                except KeyError: application.update({"Version": "Not Found"})
                try: application.update({"CPE": items["cpe"]})
                except KeyError: application.update({"CPE": "Not Found"})
                try: application.update({"hashes": items["hashes"]})
                except KeyError: application.update({"hashes": "Not Found"})
        elif items["type"] == "library":
                components.update({"Type": items["type"]}) #Required in CycloneDX
                components.update({"Name": items["name"]}) #Required in CycloneDX
                try: components.update({"Version": items["version"]})
                except KeyError: components.update({"Version": "Not Found"})
                try: components.update({"CPE": items["cpe"]})
                except KeyError: components.update({"CPE": "Not Found"})
                try: components.update({"group": items["group"]})
                except KeyError: components.update({"group": "Not Found"})
                try: components.update({"hashes": items["hashes"]})
                except KeyError: components.update({"hashes": "Not Found"})
    insertProjectIntoDB(project, application, components)


def insertProjectIntoDB(project, application, components):
    print(f"Project{project} : Application{application} : Components{components}")
    cur.execute("create table if not exists projects (owner, filename, bomVersion, bomType)")
    cur.execute("create table if not exists applications (owner, applicationName, version, hash, cpe)")
    cur.execute("create table if not exists components (owner, componentName, type, version, cpe, groups, hashes)")
    con.commit()
    cur.execute("insert into projects (owner, filename, bomVersion, bomType) values (?,?,?,?)", ("0xSpirit", project.get("projectFile"), project.get("bomVersion"), project.get("bomFormat")))
    cur.execute("insert into applications (owner, applicationName, version, hash, cpe) values (?,?,?,?,?)", ("0xSpirit", application.get("Name"), application.get("Version"), application.get("hashes"), application.get("CPE")))
    cur.execute("insert into components (owner, componentName, type, version, cpe, groups, hashes) values (?,?,?,?,?,?,?)", ("0xSpirit", components.get("Name"), components.get("Type"), components.get("Version"), components.get("CPE"), components.get("group"), components.get("hashes")))
    con.commit()

if __name__ == '__main__':
    path = "../data/projectFiles"
    testCase("testBom.json")
    #checkForNewProjects()
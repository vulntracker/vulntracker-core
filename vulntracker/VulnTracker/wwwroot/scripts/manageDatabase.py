#! /usr/bin/env python3
import sqlite3, time
con = sqlite3.connect("../data/libraries.db")
cur = con.cursor()

def createLibrariesDatabase():
    cur.execute("create table if not exists libraries (name, owner, repo, lang, githubUrl, ownerRepo)")
    con.commit()
    inFile = open("../res/libraryInfo.csv")
    count = 0
    for line in inFile:
        if count == 0:
            count = count + 1
        else:

            data = line.split(",")
            print(f"[{round((count / 5003)*100, 2)}%] Now Inserting: {data[3]}, {data[2]}, {data[3]}, {data[4]}")
            cur.execute("insert into libraries (name, owner, repo, lang, githubUrl, ownerRepo) values (?,?,?,?,?,?)", (data[3], data[2], data[3], data[4], data[1], data[0]))
            con.commit()
            count = count + 1
            time.sleep(0.25)


def createProjectDatabase():
    cur.execute("drop table if exists projects")
    cur.execute("drop table if exists applications")
    cur.execute("drop table if exists components")
    con.commit()

if __name__ == '__main__':
    createProjectDatabase()
    con.close()

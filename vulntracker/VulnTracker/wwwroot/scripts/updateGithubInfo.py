#! /usr/bin/env python3
import requests, time, sqlite3

def getLibraryFromCSV():
    inFile = open("../res/libraryInfo.csv")
    count = 0
    for line in inFile:
        checkRateLimit(count)
        if count == 0:
            count = count + 1
        else:
            count = count + 1
            data = line.split(",")
            getRepoInfo(data[2], data[3])
            print(libraries)

def getRepoInfo(owner, repo):
    repoInfoRequest = requests.get(f"{url}repos/{owner}/{repo}")
    if repoInfoRequest.status_code == 200:
        libraries.update({"lib": repoInfoRequest.json()["name"]})
        libraries.update({"topics": repoInfoRequest.json()["topics"]})
        libraries.update({"EOL": repoInfoRequest.json()["archived"]})

def checkRateLimit(count):
    rateLimitRequest = requests.get(f"{url}rate_limit")
    if rateLimitRequest.status_code == 200:
        if rateLimitRequest.json()["remaining"] <= 10:
            time.sleep(3600)
    else:
        time.sleep(600)
        checkRateLimit(count)
    
if __name__ == '__main__':
    libraries = {}
    url = "http://api.github.com/"
    getLibraryFromCSV()
    print(libraries)
#nullable disable

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace VulnTracker.Pages
{
    public class ProjectsModel : PageModel
    {
        public void OnGet()
        {
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Project Name")]
            public string ProjectName { get; set; }

            [Required]
            [DataType(DataType.Upload)]
            [Display(Name = "Project SBOM")]
            public string ProjectFile { get; set; }
        }

    }
}
